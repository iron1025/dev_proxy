/**
 * @author iHeng
 * @date 16/12/16
 */

/**
 * @interface Config
 */
interface Config {
    /* 入口页面接口 */
    start: string;
    isJoined: string;
    isSignUp: string;
    nm: string;
    join: string;
    info: string;
    /* 用户信息 */
    lotteryNm: string;
    lottery: string;
    prizeList: string;
    weekDynamicProfit: string;
    monthDynamicProfit: string;
    weekFinalProfit: string;
    currentMonthRank: string;
    isCurWeek: string;
    /* 排名页面 */
    listlist: string;
    queryquery: string;
    weekTimeweekTime: string;
    matchListmatchList: string;
    /* 新年红包中奖 */
    redbagPrizeList: string;
    userRedBagInfo: string;
    /* 协议 */
    protocal: string;
}
/**
 * @interface Utility
 */
interface Utility {
    /**
     * 获取url参数信息
     * @param {string} key
     * @memberOf Utility
     */
    getSearchValue(key: string): void;
    /**
     * 注册密码过于特殊字符验证
     * @param {string} str
     * @returns {boolean}
     * @memberOf Utility
     */
    notHasVerySpecialCode(str: string): boolean;
    /**
     * 验证手机号格式
     * @param {number} phone
     * @returns {boolean}
     * @memberOf Utility
     */
    isFormatRight(phone: number): boolean;
    /**
     * 手机号隐藏中间4位
     * @param {number} phone
     * @memberOf Utility
     */
    formatPhone(phone: number): void;
    /**
     * 显示弹出框
     * 
     * @param {string} msg
     * @param {number} time
     * 
     * @memberOf Utility
     */
    showDialog(msg: string, time: number): void;
    /**
     * 
     * 
     * @returns {boolean}
     * 
     * @memberOf Utility
     */
    canOrNotGetMsgCode(): boolean;
    /**
     * 分享链接
     * 
     * @param {string} inviteCode
     * @param {string} shareContentStr
     * @returns {string}
     * 
     * @memberOf Utility
     */
    shareLink(inviteCode: string, shareContentStr: string): string;
    /**
     * 异步获取数据
     * @param {string} url
     * @param {string} type
     * @param {string} data
     * @param {any} callback
     * @memberOf Utility
     */
    getData(url: string, type: string, data: string, callback): void;
    /**
     * 设备检查
     * @memberOf Utility
     */
    uaCheck(): void;
}
declare var config: Config;
declare var Utility: Utility;
