interface Dialog {
	content: string,
	title: string,
	time: number,
}
interface ZeptoStatic {
	dialog(options: Dialog): ZeptoStatic;
}